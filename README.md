# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

Execute este projeto em sua IDE de preferencia.

O programa se baseia em um cadastro rapido de cliente e dos produtos que esse cliente deseja e um cadastro de funcionarios.

O programa inicia em uma tela de login onde os campos devem ser preenchidos da seguinte forma: "Usuario" = Daniel e "Senha" = 123.

Com o login sendo feito um menu ira abrir com as opcoes de Pedidos ou Equipe.

Em Pedidos sera realizado o cadastro do cliente e dos produtos que esse cliente deseja.

Em Equipe sera realizado o cadastro de funcionarios.

Daniel Ashton Oda - 150008121 
