/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.util.ArrayList;
import modelo.Produto;

/**
 *
 * @author daniel
 */
public class ControleProduto {
    
    private final ArrayList<Produto> listaProdutos;
        
    public ControleProduto(){
        this.listaProdutos = new ArrayList<Produto>();
    }
    
    public ArrayList<Produto> getListaProdutos(){
        return listaProdutos;
    }
    
    public void adicionarProduto(Produto umProduto){
        listaProdutos.add(umProduto);
    }
    
    public void removerProduto(Produto umProduto){
        listaProdutos.remove(umProduto);
    }
    
    public Produto pesquisar(String nome){
        for(Produto p: listaProdutos){
            if(p.getNome().equalsIgnoreCase(nome)) return p;
        }
    return null;
    }    
} 
    

