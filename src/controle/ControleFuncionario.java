/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.util.ArrayList;
import modelo.Funcionario;

/**
 *
 * @author daniel
 */
public class ControleFuncionario {
    
    private final ArrayList<Funcionario> listaFuncionarios;
    public ControleFuncionario(){
        this.listaFuncionarios = new ArrayList<>();
    }
    public ArrayList<Funcionario> getListaFuncionarios(){
        return listaFuncionarios;
    }
    public void adicionarFuncionario(Funcionario umFuncionario){
        listaFuncionarios.add(umFuncionario);
    }
    public void removerFuncionario(Funcionario umFuncionario){
        listaFuncionarios.remove(umFuncionario);
    }
    public Funcionario pesquisar(String nome){
        for(Funcionario b: listaFuncionarios){
            if(b.getNome().equalsIgnoreCase(nome)) return b;
        }
    return null;
    }
    

}    
