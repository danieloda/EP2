/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.util.ArrayList;
import modelo.Cliente;
/**
 *
 * @author daniel
 */
public class ControleCliente {
    
    private final ArrayList<Cliente> listaClientes;
        
    public ControleCliente(){
        this.listaClientes = new ArrayList<Cliente>();
    }
    
    public ArrayList<Cliente> getListaClientes(){
        return listaClientes;
    }
    
    public void adicionarCliente(Cliente umCliente){
        listaClientes.add(umCliente);
    }
    
    public void removerCliente(Cliente umCliente){
        listaClientes.remove(umCliente);
    }
    
    public Cliente pesquisar(String nome){
        for(Cliente b: listaClientes){
            if(b.getNome().equalsIgnoreCase(nome)) return b;
        }
    return null;
    }    
} 
   /**
     *
     * @param args
     */
    
 
     