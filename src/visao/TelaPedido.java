/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visao;

import controle.ControleCliente;
import java.util.ArrayList;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import modelo.Cliente;
import modelo.Produto;
import controle.ControleProduto;
import javax.swing.JOptionPane;

/**
 *
 * @author daniel
 */
public class TelaPedido extends javax.swing.JFrame {

    private final ControleProduto controleProduto;
    private final ControleCliente controleCliente;
    private Cliente umCliente;
    private boolean novoRegistro;
    private Produto umProduto;

    /**
     * Creates new form TelaPedido
     */
    public TelaPedido() {

        initComponents();
        this.controleCliente = new ControleCliente();
        this.controleProduto = new ControleProduto();
        this.jTableListaClientes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.jTableListaProdutos.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        umCliente = null;
        umProduto = null;
        novoRegistro = true;
        this.jTextFieldNome.requestFocus();
        jTextFieldNome.setEnabled(false);
        jTextFieldNomeProduto.setEnabled(false);
        jTextFieldCpf.setEnabled(false);
        jTextFieldCep.setEnabled(false);
        jTextFieldTipo.setEnabled(false);
        jTextFieldEstoque.setEnabled(false);
        jTextFieldTelefone.setEnabled(false);
        jTextFieldNumero.setEnabled(false);
        jTextFieldComplemento.setEnabled(false);
        jTextFieldBairro.setEnabled(false);
    }

    private void carregarListaClientes() {

        ArrayList<Cliente> listaClientes = controleCliente.getListaClientes();
        DefaultTableModel model = (DefaultTableModel) jTableListaClientes.getModel();
        model.setRowCount(0);

        listaClientes.stream().forEach((c) -> {
            model.addRow(new String[]{c.getNome(), c.getTelefone(), c.getCpf()});
        });
        jTableListaClientes.setModel(model);
    }

    private void CarregarListaProdutos() {

        ArrayList<Produto> listaProdutos = controleProduto.getListaProdutos();
        DefaultTableModel model1 = (DefaultTableModel) jTableListaProdutos.getModel();
        model1.setRowCount(0);
        listaProdutos.stream().forEach((p) -> {
            model1.addRow(new String[]{p.getNome(), p.getTipo(), String.valueOf(p.getEstoque())});
        });
        jTableListaProdutos.setModel(model1);
    }

    private void preencherCamposCliente() {

        jTextFieldNome.setText(umCliente.getNome());
        jTextFieldCpf.setText(umCliente.getCpf());
        jTextFieldTelefone.setText(umCliente.getTelefone());
        jTextFieldCep.setText(umCliente.getCep());
        jTextFieldNumero.setText(umCliente.getNumero());
        jTextFieldComplemento.setText(umCliente.getComplemento());
        jTextFieldBairro.setText(umCliente.getBairro());
    }

    private void preencherCamposProduto() {

        jTextFieldNomeProduto.setText(umProduto.getNome());
        jTextFieldTipo.setText(umProduto.getTipo());
        jTextFieldEstoque.setText(String.valueOf(umProduto.getEstoque()));

    }

    private void pesquisarCliente(String nome) {

        Cliente clientePesquisado = controleCliente.pesquisar(nome);
        this.umCliente = clientePesquisado;
        this.preencherCamposCliente();
    }

    private void pesquisarProduto(String nome) {

        Produto produtoPesquisado = controleProduto.pesquisar(nome);
        this.umProduto = produtoPesquisado;
        this.preencherCamposProduto();

    }

    private void salvarCliente() {

        if (novoRegistro == true) {
            umCliente = new Cliente(jTextFieldNome.getText());
        } else {
            umCliente.setNome(jTextFieldNome.getText());
        }

        umCliente.setTelefone(jTextFieldTelefone.getText());
        umCliente.setCpf(jTextFieldCpf.getText());
        umCliente.setBairro(jTextFieldBairro.getText());
        umCliente.setNumero(jTextFieldNumero.getText());
        umCliente.setCep(jTextFieldCep.getText());
        umCliente.setComplemento(jTextFieldComplemento.getText());
        controleCliente.adicionarCliente(umCliente);
        novoRegistro = false;
        this.carregarListaClientes();
        jTextFieldNome.setText(null);

        jTextFieldNome.setEnabled(false);
        jTextFieldCpf.setEnabled(false);
        jTextFieldCep.setEnabled(false);
        jTextFieldTelefone.setEnabled(false);
        jTextFieldNumero.setEnabled(false);
        jTextFieldComplemento.setEnabled(false);
        jTextFieldBairro.setEnabled(false);
    }

    private void salvarProduto() {

        if (novoRegistro == true) {
            umProduto = new Produto(jTextFieldNomeProduto.getText());
        } else {
            umProduto.setNome(jTextFieldNomeProduto.getText());
        }

        umProduto.setEstoque(Integer.parseInt(jTextFieldEstoque.getText()));
        umProduto.setTipo(jTextFieldTipo.getText());
        controleProduto.adicionarProduto(umProduto);
        novoRegistro = false;
        this.CarregarListaProdutos();
        jTextFieldNomeProduto.setText(null);
        jTextFieldNomeProduto.setEnabled(false);
        jTextFieldTipo.setEnabled(false);
        jTextFieldEstoque.setEnabled(false);

    }

    private void limparCamposCliente() {

        jTextFieldNome.setText(null);
        jTextFieldCpf.setText(null);
        jTextFieldTelefone.setText(null);
        jTextFieldCep.setText(null);
        jTextFieldNumero.setText(null);
        jTextFieldComplemento.setText(null);
        jTextFieldBairro.setText(null);

    }

    private void limparCamposProduto() {

        jTextFieldNomeProduto.setText(null);
        jTextFieldEstoque.setText(null);
        jTextFieldTipo.setText(null);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonCancelar = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableListaClientes = new javax.swing.JTable();
        jLabelListaBoxeadores = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jTextFieldNomeProduto = new javax.swing.JTextField();
        jLabelNome = new javax.swing.JLabel();
        jLabelRg = new javax.swing.JLabel();
        jLabelCpf = new javax.swing.JLabel();
        jTextFieldTipo = new javax.swing.JTextField();
        jTextFieldEstoque = new javax.swing.JTextField();
        jButtonNovo1 = new javax.swing.JButton();
        jButtonAlterar1 = new javax.swing.JButton();
        jButtonExcluir1 = new javax.swing.JButton();
        jButtonSalvar1 = new javax.swing.JButton();
        jButtonRealizar = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTableListaProdutos = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jTextFieldNome = new javax.swing.JTextField();
        jLabelNome1 = new javax.swing.JLabel();
        jLabelRg1 = new javax.swing.JLabel();
        jLabelCpf1 = new javax.swing.JLabel();
        jTextFieldTelefone = new javax.swing.JTextField();
        jTextFieldCpf = new javax.swing.JTextField();
        jButtonNovo = new javax.swing.JButton();
        jButtonAlterar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabelNumero1 = new javax.swing.JLabel();
        jTextFieldNumero = new javax.swing.JTextField();
        jTextFieldBairro = new javax.swing.JTextField();
        jLabelBairro1 = new javax.swing.JLabel();
        jLabelComplemento1 = new javax.swing.JLabel();
        jTextFieldComplemento = new javax.swing.JTextField();
        jLabelCep1 = new javax.swing.JLabel();
        jTextFieldCep = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jTableListaClientes.setModel(new javax.swing.table.DefaultTableModel
            (
                null,
                new String [] {
                    "Nome", "CPF", "Telefone"
                }
            )
            {
                @Override
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            jTableListaClientes.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jTableListaClientesMouseClicked(evt);
                }
            });
            jTableListaClientes.addComponentListener(new java.awt.event.ComponentAdapter() {
                public void componentShown(java.awt.event.ComponentEvent evt) {
                    jTableListaClientesComponentShown(evt);
                }
            });
            jScrollPane4.setViewportView(jTableListaClientes);

            jLabelListaBoxeadores.setText("Lista de Clientes:");

            jTextFieldNomeProduto.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jTextFieldNomeProdutoActionPerformed(evt);
                }
            });

            jLabelNome.setText("Nome:");

            jLabelRg.setText("Tipo:");

            jLabelCpf.setText("Estoque:");

            jButtonNovo1.setText("Novo");
            jButtonNovo1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonNovo1ActionPerformed(evt);
                }
            });

            jButtonAlterar1.setText("Alterar");
            jButtonAlterar1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonAlterar1ActionPerformed(evt);
                }
            });

            jButtonExcluir1.setText("Excluir");
            jButtonExcluir1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonExcluir1ActionPerformed(evt);
                }
            });

            jButtonSalvar1.setText("Salvar");
            jButtonSalvar1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonSalvar1ActionPerformed(evt);
                }
            });

            jButtonRealizar.setText("Realizar Pedido");
            jButtonRealizar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonRealizarActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
            jPanel1.setLayout(jPanel1Layout);
            jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap(21, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabelNome, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                                    .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabelRg, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jButtonNovo1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jButtonAlterar1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jButtonExcluir1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jButtonSalvar1))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabelCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(18, 18, 18))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jButtonRealizar)
                            .addContainerGap())))
            );
            jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonNovo1)
                        .addComponent(jButtonAlterar1)
                        .addComponent(jButtonExcluir1)
                        .addComponent(jButtonSalvar1))
                    .addGap(18, 18, 18)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelNome)
                        .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabelRg))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelCpf)
                        .addComponent(jTextFieldEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
                    .addComponent(jButtonRealizar, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
            );

            jTabbedPane1.addTab("Informações Gerais", jPanel1);

            jTableListaProdutos.setModel(new javax.swing.table.DefaultTableModel
                (
                    null,
                    new String [] {
                        "Nome", "Tipo", "Estoque"
                    }
                )
                {
                    @Override
                    public boolean isCellEditable(int rowIndex, int mColIndex) {
                        return false;
                    }
                });
                jTableListaProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        jTableListaProdutosMouseClicked(evt);
                    }
                });
                jTableListaProdutos.addComponentListener(new java.awt.event.ComponentAdapter() {
                    public void componentShown(java.awt.event.ComponentEvent evt) {
                        jTableListaProdutosComponentShown(evt);
                    }
                });
                jScrollPane5.setViewportView(jTableListaProdutos);

                jLabel1.setText("Lista Produtos:");

                jTextFieldNome.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jTextFieldNomeActionPerformed(evt);
                    }
                });

                jLabelNome1.setText("Nome:");

                jLabelRg1.setText("Telefone:");

                jLabelCpf1.setText("CPF:");

                jButtonNovo.setText("Novo");
                jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButtonNovoActionPerformed(evt);
                    }
                });

                jButtonAlterar.setText("Alterar");
                jButtonAlterar.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButtonAlterarActionPerformed(evt);
                    }
                });

                jButtonExcluir.setText("Excluir");
                jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButtonExcluirActionPerformed(evt);
                    }
                });

                jButtonSalvar.setText("Salvar");
                jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButtonSalvarActionPerformed(evt);
                    }
                });

                javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
                jPanel3.setLayout(jPanel3Layout);
                jPanel3Layout.setHorizontalGroup(
                    jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(jLabelCpf1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(28, 28, 28))
                                    .addComponent(jLabelRg1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabelNome1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonAlterar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonExcluir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonSalvar)))
                        .addContainerGap(22, Short.MAX_VALUE))
                );
                jPanel3Layout.setVerticalGroup(
                    jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonNovo)
                            .addComponent(jButtonAlterar)
                            .addComponent(jButtonExcluir)
                            .addComponent(jButtonSalvar))
                        .addGap(16, 16, 16)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNome1)
                            .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelRg1)
                            .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCpf1)
                            .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(151, Short.MAX_VALUE))
                );

                jTabbedPane2.addTab("Informações Gerais", jPanel3);

                jLabelNumero1.setText("Número:");

                jLabelBairro1.setText("Bairro:");

                jLabelComplemento1.setText("Complemento:");

                jLabelCep1.setText("CEP:");

                javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
                jPanel4.setLayout(jPanel4Layout);
                jPanel4Layout.setHorizontalGroup(
                    jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelComplemento1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelBairro1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelNumero1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelCep1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
                            .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldCep, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                );
                jPanel4Layout.setVerticalGroup(
                    jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCep1)
                            .addComponent(jTextFieldCep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNumero1)
                            .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelComplemento1)
                            .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelBairro1)
                            .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(288, 288, 288))
                );

                jTabbedPane2.addTab("Endereço", jPanel4);

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabelListaBoxeadores)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(jTabbedPane2)
                                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                                .addGap(33, 33, 33)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel1)
                                    .addComponent(jTabbedPane1)
                                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                                .addContainerGap(13, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                );
                layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButtonCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelListaBoxeadores)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23))
                );

                setSize(new java.awt.Dimension(716, 609));
                setLocationRelativeTo(null);
            }// </editor-fold>//GEN-END:initComponents

    private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed

        jButtonNovo.setEnabled(false);
        umCliente = null;
        novoRegistro = true;
        this.jTextFieldNome.requestFocus();
        this.limparCamposCliente();

        jTextFieldNome.setEnabled(true);
        jTextFieldCpf.setEnabled(true);
        jTextFieldCep.setEnabled(true);
        jTextFieldTelefone.setEnabled(true);
        jTextFieldNumero.setEnabled(true);
        jTextFieldComplemento.setEnabled(true);
        jTextFieldBairro.setEnabled(true);

    }//GEN-LAST:event_jButtonNovoActionPerformed

    private void jButtonAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAlterarActionPerformed

        novoRegistro = false;
        this.jTextFieldNome.requestFocus();
        this.preencherCamposCliente();
        jTextFieldNome.setEnabled(true);
        jTextFieldCpf.setEnabled(true);
        jTextFieldCep.setEnabled(true);
        jTextFieldTelefone.setEnabled(true);
        jTextFieldNumero.setEnabled(true);
        jTextFieldComplemento.setEnabled(true);
        jTextFieldBairro.setEnabled(true);

    }//GEN-LAST:event_jButtonAlterarActionPerformed

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
        this.controleCliente.removerCliente(umCliente);
        umCliente = null;
        this.carregarListaClientes();
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        this.salvarCliente();
        this.limparCamposCliente();
        jButtonNovo.setEnabled(true);
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        TelaPrincipal telaP = new TelaPrincipal();
        telaP.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jTableListaClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaClientesMouseClicked
        if (jTableListaClientes.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableListaClientes.getModel();
            String nome = (String) model.getValueAt(jTableListaClientes.getSelectedRow(), 0);
            this.pesquisarCliente(nome);
        }
    }//GEN-LAST:event_jTableListaClientesMouseClicked

    private void jTableListaClientesComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jTableListaClientesComponentShown
        // TODO add your handling code here:
    }//GEN-LAST:event_jTableListaClientesComponentShown

    private void jTableListaProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaProdutosMouseClicked
        if (jTableListaProdutos.isEnabled()) {
            DefaultTableModel model1 = (DefaultTableModel) jTableListaProdutos.getModel();
            String nome = (String) model1.getValueAt(jTableListaProdutos.getSelectedRow(), 0);
            this.pesquisarProduto(nome);
        }
    }//GEN-LAST:event_jTableListaProdutosMouseClicked

    private void jTableListaProdutosComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jTableListaProdutosComponentShown
        // TODO add your handling code here:
    }//GEN-LAST:event_jTableListaProdutosComponentShown

    private void jTextFieldNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNomeActionPerformed

    private void jTextFieldNomeProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNomeProdutoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNomeProdutoActionPerformed

    private void jButtonNovo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovo1ActionPerformed
        umProduto = null;
        novoRegistro = true;
        this.jTextFieldNomeProduto.requestFocus();
        this.limparCamposProduto();
        jButtonNovo1.setEnabled(false);
        jTextFieldNomeProduto.setText(null);

        jTextFieldNomeProduto.setEnabled(true);
        jTextFieldTipo.setEnabled(true);
        jTextFieldEstoque.setEnabled(true);
    }//GEN-LAST:event_jButtonNovo1ActionPerformed

    private void jButtonAlterar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAlterar1ActionPerformed
        novoRegistro = false;
        this.jTextFieldNomeProduto.requestFocus();
        jTextFieldNomeProduto.setEnabled(true);
        jTextFieldTipo.setEnabled(true);
        jTextFieldEstoque.setEnabled(true);
        this.preencherCamposProduto();
    }//GEN-LAST:event_jButtonAlterar1ActionPerformed

    private void jButtonExcluir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluir1ActionPerformed
        this.controleProduto.removerProduto(umProduto);
        umProduto = null;
        this.CarregarListaProdutos();

    }//GEN-LAST:event_jButtonExcluir1ActionPerformed

    private void jButtonSalvar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvar1ActionPerformed
        this.salvarProduto();
        this.limparCamposProduto();
        jButtonNovo1.setEnabled(true);

    }//GEN-LAST:event_jButtonSalvar1ActionPerformed

    private void jButtonRealizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRealizarActionPerformed

        String auxNome = umCliente.getNome();
        String auxCpf = umCliente.getCpf();
        StringBuilder products = new StringBuilder();

        controleProduto.getListaProdutos().stream().forEach((produto) -> {
            products.append(produto.getNome()).append("\n");
        });
        JOptionPane.showMessageDialog(this, " Nome: " + auxNome + "\n CPF: " + auxCpf + "\n Produtos: \n" + " " + products);


    }//GEN-LAST:event_jButtonRealizarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new TelaPedido().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAlterar;
    private javax.swing.JButton jButtonAlterar1;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonExcluir1;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonNovo1;
    private javax.swing.JButton jButtonRealizar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JButton jButtonSalvar1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelBairro1;
    private javax.swing.JLabel jLabelCep1;
    private javax.swing.JLabel jLabelComplemento1;
    private javax.swing.JLabel jLabelCpf;
    private javax.swing.JLabel jLabelCpf1;
    private javax.swing.JLabel jLabelListaBoxeadores;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelNome1;
    private javax.swing.JLabel jLabelNumero1;
    private javax.swing.JLabel jLabelRg;
    private javax.swing.JLabel jLabelRg1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTableListaClientes;
    private javax.swing.JTable jTableListaProdutos;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldCep;
    private javax.swing.JTextField jTextFieldComplemento;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldEstoque;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNomeProduto;
    private javax.swing.JTextField jTextFieldNumero;
    private javax.swing.JTextField jTextFieldTelefone;
    private javax.swing.JTextField jTextFieldTipo;
    // End of variables declaration//GEN-END:variables
}
